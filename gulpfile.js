var gulp 			    = require('gulp');
var browserSync 	= require('browser-sync');
var inject 			  = require('gulp-inject');
var sass			    = require('gulp-sass');
var minifyCss		  = require('gulp-clean-css');
var source 			  = require('vinyl-source-stream');
var notify			  = require('gulp-notify');
var jshint			  = require('gulp-jshint');
var watch         = require('gulp-watch');
var browserify 		= require('browserify');
var bowerFiles    = require('main-bower-files');

var developmentRoot = './src';
var productionRoot 	=  './build';

//Static resource to be injected into html page

var injectionFiles = [ 
  './src/lib/**.css',
	'./src/css/**.css',
	'./src/js/travelfix-bundle.js']


gulp.task('sass', function () {
  return gulp.src( developmentRoot + '/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest( developmentRoot + '/css' ))
    .pipe(browserSync.stream());
});

gulp.task('minifyCss', ['sass'], function () {
	return gulp.src(developmentRoot + '/css/*.css')
	.pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest( productionRoot + '/css' ));
});

gulp.task('lint', function () {
	return gulp.src(developmentRoot + '/js/*.js')
	.pipe(jshint())
    .pipe(jshint.reporter('default'));
});

//Bundle all the required JS Files
gulp.task('browserify', ['lint'], function () {
	return browserify(developmentRoot+'/js/travelfix-bundle.js')
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        .pipe(source('travelfix-bundle.js'))
        // Start piping stream to tasks!
        .pipe(notify('AppJS Bundled Successfully'))
        .pipe(gulp.dest(productionRoot+'/js/'))
        .pipe(browserSync.stream());
});

//Extract the static files from bower and src folder into build folder
gulp.task('extractBowerFiles', function () {
  return gulp.src(bowerFiles({  paths: {
        bowerrc: './.bowerrc'
    }}))
  .pipe(gulp.dest(developmentRoot +'/lib') );
});

gulp.task('imagesExtract', function () {
  return gulp.src([ developmentRoot+'/fonts/*.*'])
  .pipe(gulp.dest(productionRoot + '/fonts'));
})

gulp.task('fontExtract', function () {
  return gulp.src([ developmentRoot+'/images/*'])
  .pipe(gulp.dest(productionRoot + '/images'));
})

gulp.task('distBowerFiles', ['imagesExtract', 'fontExtract', 'extractBowerFiles'], function () {
  return gulp.src(developmentRoot + '/lib/*.*')
  .pipe(gulp.dest(productionRoot +'/lib'));
});


// Inject the needed files into index html, we use the injection variable here

gulp.task('index', ['distBowerFiles' ,'browserify', 'minifyCss'], function () {
  
  var target = gulp.src(developmentRoot + '/*.html');
  // It's not necessary to read the files (will speed up things), we're only after their paths: 
  var sources = gulp.src(injectionFiles, {read: false});
  return target.pipe(inject(sources, {
    relative: false,
    addRootSlash: false,
    transform: function(filepath) {
        var RH = filepath.substring(3, filepath.length);
        if (filepath.slice(-3) === '.js') {
          var result = "<script type=\"text/javascript\" src=\"" + RH + "\"></script>";
          return result;
        } else {
          var result = "<link rel=\"stylesheet\" href=\"" + RH + "\">";
          return result;
        } 
    }
  
}))
    .pipe(gulp.dest(productionRoot));
});

// Static Server + watching scss/html files
gulp.task('serve', ['index'], function() {

    browserSync.init({
        server: productionRoot
    });

    gulp.watch(developmentRoot + "/scss/*.scss", ['sass']);
    gulp.watch('./libraries', ['distBowerFiles', 'index']);
    gulp.watch(developmentRoot + "/js/*.js", ['browserify']);
    gulp.watch(developmentRoot +'/*.html', ['index'])
    gulp.watch(productionRoot+ "/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);

