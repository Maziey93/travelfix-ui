//<![CDATA[
$(window).load(function(){
	$('.radio-tabs .radio-custom').on('click', function(){
		$('.radio-tabs .radio-custom.current').removeClass('current');
		$(this).addClass('current');
	});

	$("#range_121").ionRangeSlider({
			type: "double",
			min: 0,
			max: 100,
			from: 30,
			to: 70,
			drag_interval: true
	});
});//]]> 

$(document).ready(function () {
		
	$(".toggle-header").click(function() {
		$(".chating-wrap").toggleClass("spanner");
	});
			
});

$('.responsive-tabs').responsiveTabs({
	accordionOn: ['xs', 'sm']
});
$('input[name="intervaltype"]').click(function () {
	$(this).tab('show');
});

$(".popp").mouseover(function() {
	  $(".flight-popup").show();
});
$(".closep").click(function() {
	  $(".flight-popup").hide();
});
$(window).scroll(function() {    
   var scroll = $(window).scrollTop();

	if (scroll > 1) {
		$("#bs-navbar").addClass("sticky");
	} else {
		$("#bs-navbar").removeClass("sticky");
	}
});
