TRAVELFIX UI REMAKE
===================

SET UP
-----

* clone the project, cd into the product directory
* run ```npm install && bower install``` to install project dependencies
* switch to the development branch
* then run ```gulp``` to start a server and view the application.

the set up instructions are provided under the assumption that node is already installed on the machine, otherwise [download](http://nodejs.org/download) node, then install gulp globally ```npm install gulp -g```  

CASES STUDIES
-------------

* [Speckyboy](http://speckyboy.com/2015/03/09/the-essentials-of-designing-hotel-reservation-interfaces/)
* [uxbooth](http://www.uxbooth.com/articles/hotel-booking-from-start-to-finish/)
* [designmodo](http://designmodo.com/online-hotel-booking/)
* [kayak](https://www.kayak.com/flights/LOS-ABV/2016-03-13/2016-03-23)
* [Splacer](https://www.splacer.co)

Inspiration
* [Uplabs](http://www.site.uplabs.com/posts/zo-rooms-website)
